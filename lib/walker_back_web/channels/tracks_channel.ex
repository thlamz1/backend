defmodule WalkerBackWeb.TracksChannel do
  use WalkerBackWeb, :channel
  alias WalkerBack.Tracks
  alias WalkerBackWeb.PathView
  @impl true
  def join("tracks:list", _payload, socket) do
    paths = Tracks.list_paths()
    {:ok, PathView.render("index.json", paths: paths), socket}
  end

  @impl true
  def join("tracks:" <> _other, _payload, _socket) do
    {:error, %{reason: "unauthorized"}}
  end


  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  @impl true
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (tracks:lobby).
  @impl true
  def handle_in("shout", payload, socket) do
    broadcast socket, "shout", payload
    {:noreply, socket}
  end
end

defmodule WalkerBackWeb.PathController do
  use WalkerBackWeb, :controller

  alias WalkerBack.Tracks
  alias WalkerBack.Tracks.Path
  alias WalkerBackWeb.Endpoint
  alias WalkerBackWeb.PathView

  action_fallback WalkerBackWeb.FallbackController

  def index(conn, _params) do
    paths = Tracks.list_paths()
    render(conn, "index.json", paths: paths)
  end

  def create(conn, %{"path" => path_params}) do
    with {:ok, %Path{} = path} <- Tracks.create_path(path_params) do
      if Map.has_key?(path_params, "waypoints") do
        for waypoint <- path_params["waypoints"] do
          Map.put(waypoint, "path_id", path.id)
          |> Tracks.create_waypoint()
        end
      end

      path = Tracks.get_path!(path.id)

      # Broadcasting to socket
      Endpoint.broadcast!("tracks:list", "newTrack", PathView.render("show.json", path: path))

      conn
      |> put_status(:created)
      |> render("show.json", path: path)
    end
  end

  def show(conn, %{"id" => id}) do
    path = Tracks.get_path!(id)
    render(conn, "show.json", path: path)
  end

  def update(conn, %{"id" => id, "path" => path_params}) do
    path = Tracks.get_path!(id)

    if Map.has_key?(path_params, "waypoints") do
      for waypoint <- path.waypoints do
        if !Enum.any?(path_params["waypoints"], &(Map.has_key?(&1, "id") && waypoint.id == &1["id"])) do
          Tracks.get_waypoint!(waypoint.id)
          |> Tracks.delete_waypoint()
        end
      end


      Enum.each(path_params["waypoints"], fn(waypoint) ->
        if Map.has_key?(waypoint, "id") and waypoint["id"] do
            Tracks.get_waypoint!(waypoint["id"])
            |> Tracks.update_waypoint(waypoint)
        else
            Map.put(waypoint, "path_id", path.id)
            |> Tracks.create_waypoint()
        end
      end)

    end

    with {:ok, %Path{} = path} <- Tracks.update_path(path, path_params) do
      path = Tracks.get_path!(path.id)

      # Broadcasting to socket
      Endpoint.broadcast!("tracks:list", "updateTrack", PathView.render("show.json", path: path))

      render(conn, "show.json", path: path)
    end
  end

  def delete(conn, %{"id" => id}) do
    path = Tracks.get_path!(id)

    with {:ok, %Path{}} <- Tracks.delete_path(path) do

      # Broadcasting to socket
      Endpoint.broadcast!("tracks:list", "removeTrack", %{data: id})

      send_resp(conn, :no_content, "")
    end
  end
end

defmodule WalkerBackWeb.WaypointController do
  use WalkerBackWeb, :controller

  alias WalkerBack.Tracks
  alias WalkerBack.Tracks.Waypoint

  action_fallback WalkerBackWeb.FallbackController

  def index(conn, _params) do
    waypoints = Tracks.list_waypoints()
    render(conn, "index.json", waypoints: waypoints)
  end

  def create(conn, %{"waypoint" => waypoint_params}) do
    with {:ok, %Waypoint{} = waypoint} <- Tracks.create_waypoint(waypoint_params) do
      conn
      |> put_status(:created)
      |> render("show.json", waypoint: waypoint)
    end
  end

  def show(conn, %{"id" => id}) do
    waypoint = Tracks.get_waypoint!(id)
    render(conn, "show.json", waypoint: waypoint)
  end

  def update(conn, %{"id" => id, "waypoint" => waypoint_params}) do
    waypoint = Tracks.get_waypoint!(id)

    with {:ok, %Waypoint{} = waypoint} <- Tracks.update_waypoint(waypoint, waypoint_params) do
      render(conn, "show.json", waypoint: waypoint)
    end
  end

  def delete(conn, %{"id" => id}) do
    waypoint = Tracks.get_waypoint!(id)

    with {:ok, %Waypoint{}} <- Tracks.delete_waypoint(waypoint) do
      send_resp(conn, :no_content, "")
    end
  end
end

defmodule WalkerBackWeb.PageController do
  use WalkerBackWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end

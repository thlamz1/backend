defmodule WalkerBackWeb.Router do
  use WalkerBackWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", WalkerBackWeb do
    pipe_through :api

    resources "/paths", PathController
    resources "/waypoints", WaypointController
  end
end

defmodule WalkerBackWeb.WaypointView do
  use WalkerBackWeb, :view
  alias WalkerBackWeb.WaypointView

  def render("index.json", %{waypoints: waypoints}) do
    %{data: render_many(waypoints, WaypointView, "waypoint.json")}
  end

  def render("show.json", %{waypoint: waypoint}) do
    %{data: render_one(waypoint, WaypointView, "waypoint.json")}
  end

  def render("waypoint.json", %{waypoint: waypoint}) do
    %{id: waypoint.id,
      titulo: waypoint.titulo,
      descricao: waypoint.descricao,
      thumbnail: waypoint.thumbnail,
      pathTo: waypoint.pathTo}
  end
end

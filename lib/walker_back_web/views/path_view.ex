defmodule WalkerBackWeb.PathView do
  use WalkerBackWeb, :view
  alias WalkerBackWeb.PathView

  def render("index.json", %{paths: paths}) do
    %{data: render_many(paths, PathView, "path.json")}
  end

  def render("show.json", %{path: path}) do
    %{data: render_one(path, PathView, "path.json")}
  end

  def render("path.json", %{path: path}) do
    %{id: path.id,
      titulo: path.titulo,
      descricao: path.descricao,
      categorias: path.categorias,
      thumbnail: path.thumbnail,
      waypoints: WalkerBackWeb.WaypointView.render("index.json", path)
    }
  end
end

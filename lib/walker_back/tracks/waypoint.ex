defmodule WalkerBack.Tracks.Waypoint do
  use Ecto.Schema
  import Ecto.Changeset
  alias WalkerBack.Tracks
  schema "waypoints" do
    field :descricao, :string
    field :pathTo, {:array, :string}
    field :titulo, :string
    field :thumbnail, :binary
    belongs_to :path, Tracks.Path

    timestamps()
  end

  @doc false
  def changeset(waypoint, attrs) do
    waypoint
    |> cast(attrs, [:titulo, :descricao, :pathTo, :thumbnail, :path_id])
    |> validate_required([:titulo, :descricao, :pathTo])
  end
end

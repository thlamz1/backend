defmodule WalkerBack.Tracks.Path do
  use Ecto.Schema
  import Ecto.Changeset
  alias WalkerBack.Tracks

  schema "paths" do
    field :categorias, {:array, :string}
    field :descricao, :string
    field :thumbnail, :binary
    field :titulo, :string
    has_many :waypoints, Tracks.Waypoint
    timestamps()
  end

  @doc false
  def changeset(path, attrs) do
    path
    |> cast(attrs, [:titulo, :descricao, :categorias, :thumbnail])
    |> validate_required([:titulo, :descricao])
  end
end

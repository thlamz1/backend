defmodule WalkerBack.Repo do
  use Ecto.Repo,
    otp_app: :walker_back,
    adapter: Ecto.Adapters.Postgres
end

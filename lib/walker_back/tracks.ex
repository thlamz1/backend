defmodule WalkerBack.Tracks do
  @moduledoc """
  The Tracks context.
  """

  import Ecto.Query, warn: false
  alias WalkerBack.Repo

  alias WalkerBack.Tracks.Path

  @doc """
  Returns the list of paths.

  ## Examples

      iex> list_paths()
      [%Path{}, ...]

  """
  def list_paths do
    Repo.all(Path) |> Repo.preload(:waypoints)
  end

  @doc """
  Gets a single path.

  Raises `Ecto.NoResultsError` if the Path does not exist.

  ## Examples

      iex> get_path!(123)
      %Path{}

      iex> get_path!(456)
      ** (Ecto.NoResultsError)

  """
  def get_path!(id), do: Repo.get!(Path, id) |> Repo.preload(:waypoints)

  @doc """
  Creates a path.

  ## Examples

      iex> create_path(%{field: value})
      {:ok, %Path{}}

      iex> create_path(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_path(attrs \\ %{}) do
    %Path{}
    |> Path.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a path.

  ## Examples

      iex> update_path(path, %{field: new_value})
      {:ok, %Path{}}

      iex> update_path(path, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_path(%Path{} = path, attrs) do
    path
    |> Path.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a path.

  ## Examples

      iex> delete_path(path)
      {:ok, %Path{}}

      iex> delete_path(path)
      {:error, %Ecto.Changeset{}}

  """
  def delete_path(%Path{} = path) do
    Repo.delete(path)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking path changes.

  ## Examples

      iex> change_path(path)
      %Ecto.Changeset{data: %Path{}}

  """
  def change_path(%Path{} = path, attrs \\ %{}) do
    Path.changeset(path, attrs)
  end

  alias WalkerBack.Tracks.Waypoint

  @doc """
  Returns the list of waypoints.

  ## Examples

      iex> list_waypoints()
      [%Waypoint{}, ...]

  """
  def list_waypoints do
    Repo.all(Waypoint)
  end

  @doc """
  Gets a single waypoint.

  Raises `Ecto.NoResultsError` if the Waypoint does not exist.

  ## Examples

      iex> get_waypoint!(123)
      %Waypoint{}

      iex> get_waypoint!(456)
      ** (Ecto.NoResultsError)

  """
  def get_waypoint!(id), do: Repo.get!(Waypoint, id)

  @doc """
  Creates a waypoint.

  ## Examples

      iex> create_waypoint(%{field: value})
      {:ok, %Waypoint{}}

      iex> create_waypoint(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_waypoint(attrs \\ %{}) do
    %Waypoint{}
    |> Waypoint.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a waypoint.

  ## Examples

      iex> update_waypoint(waypoint, %{field: new_value})
      {:ok, %Waypoint{}}

      iex> update_waypoint(waypoint, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_waypoint(%Waypoint{} = waypoint, attrs) do
    waypoint
    |> Waypoint.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a waypoint.

  ## Examples

      iex> delete_waypoint(waypoint)
      {:ok, %Waypoint{}}

      iex> delete_waypoint(waypoint)
      {:error, %Ecto.Changeset{}}

  """
  def delete_waypoint(%Waypoint{} = waypoint) do
    Repo.delete(waypoint)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking waypoint changes.

  ## Examples

      iex> change_waypoint(waypoint)
      %Ecto.Changeset{data: %Waypoint{}}

  """
  def change_waypoint(%Waypoint{} = waypoint, attrs \\ %{}) do
    Waypoint.changeset(waypoint, attrs)
  end
end

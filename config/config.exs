# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :walker_back,
  ecto_repos: [WalkerBack.Repo]

# Configures the endpoint
config :walker_back, WalkerBackWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "prpVrC+BWg/xpKlCPWP4MYn7svY0tO9+73T57qwV8yBkqnlSrtJEEK+teXPcza1Q",
  render_errors: [view: WalkerBackWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: WalkerBack.PubSub,
  live_view: [signing_salt: "bW2us+OK"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

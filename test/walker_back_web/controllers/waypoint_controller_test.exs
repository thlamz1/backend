defmodule WalkerBackWeb.WaypointControllerTest do
  use WalkerBackWeb.ConnCase

  alias WalkerBack.Tracks
  alias WalkerBack.Tracks.Waypoint

  @create_attrs %{
    descricao: "some descricao",
    pathTo: [],
    titulo: "some titulo"
  }
  @update_attrs %{
    descricao: "some updated descricao",
    pathTo: [],
    titulo: "some updated titulo"
  }
  @invalid_attrs %{descricao: nil, pathTo: nil, titulo: nil}

  def fixture(:waypoint) do
    {:ok, waypoint} = Tracks.create_waypoint(@create_attrs)
    waypoint
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all waypoints", %{conn: conn} do
      conn = get(conn, Routes.waypoint_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create waypoint" do
    test "renders waypoint when data is valid", %{conn: conn} do
      conn = post(conn, Routes.waypoint_path(conn, :create), waypoint: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.waypoint_path(conn, :show, id))

      assert %{
               "id" => id,
               "descricao" => "some descricao",
               "pathTo" => [],
               "titulo" => "some titulo"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.waypoint_path(conn, :create), waypoint: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update waypoint" do
    setup [:create_waypoint]

    test "renders waypoint when data is valid", %{conn: conn, waypoint: %Waypoint{id: id} = waypoint} do
      conn = put(conn, Routes.waypoint_path(conn, :update, waypoint), waypoint: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.waypoint_path(conn, :show, id))

      assert %{
               "id" => id,
               "descricao" => "some updated descricao",
               "pathTo" => [],
               "titulo" => "some updated titulo"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, waypoint: waypoint} do
      conn = put(conn, Routes.waypoint_path(conn, :update, waypoint), waypoint: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete waypoint" do
    setup [:create_waypoint]

    test "deletes chosen waypoint", %{conn: conn, waypoint: waypoint} do
      conn = delete(conn, Routes.waypoint_path(conn, :delete, waypoint))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.waypoint_path(conn, :show, waypoint))
      end
    end
  end

  defp create_waypoint(_) do
    waypoint = fixture(:waypoint)
    %{waypoint: waypoint}
  end
end

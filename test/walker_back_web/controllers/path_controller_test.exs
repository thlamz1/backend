defmodule WalkerBackWeb.PathControllerTest do
  use WalkerBackWeb.ConnCase

  alias WalkerBack.Tracks
  alias WalkerBack.Tracks.Path

  @create_attrs %{
    categorias: [],
    descricao: "some descricao",
    thumbnail: "some thumbnail",
    titulo: "some titulo"
  }
  @update_attrs %{
    categorias: [],
    descricao: "some updated descricao",
    thumbnail: "some updated thumbnail",
    titulo: "some updated titulo"
  }
  @invalid_attrs %{categorias: nil, descricao: nil, thumbnail: nil, titulo: nil}

  def fixture(:path) do
    {:ok, path} = Tracks.create_path(@create_attrs)
    path
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all paths", %{conn: conn} do
      conn = get(conn, Routes.path_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create path" do
    test "renders path when data is valid", %{conn: conn} do
      conn = post(conn, Routes.path_path(conn, :create), path: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.path_path(conn, :show, id))

      assert %{
               "id" => id,
               "categorias" => [],
               "descricao" => "some descricao",
               "thumbnail" => "some thumbnail",
               "titulo" => "some titulo"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.path_path(conn, :create), path: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update path" do
    setup [:create_path]

    test "renders path when data is valid", %{conn: conn, path: %Path{id: id} = path} do
      conn = put(conn, Routes.path_path(conn, :update, path), path: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.path_path(conn, :show, id))

      assert %{
               "id" => id,
               "categorias" => [],
               "descricao" => "some updated descricao",
               "thumbnail" => "some updated thumbnail",
               "titulo" => "some updated titulo"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, path: path} do
      conn = put(conn, Routes.path_path(conn, :update, path), path: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete path" do
    setup [:create_path]

    test "deletes chosen path", %{conn: conn, path: path} do
      conn = delete(conn, Routes.path_path(conn, :delete, path))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.path_path(conn, :show, path))
      end
    end
  end

  defp create_path(_) do
    path = fixture(:path)
    %{path: path}
  end
end

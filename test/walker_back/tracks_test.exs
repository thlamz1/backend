defmodule WalkerBack.TracksTest do
  use WalkerBack.DataCase

  alias WalkerBack.Tracks

  describe "paths" do
    alias WalkerBack.Tracks.Path

    @valid_attrs %{categorias: [], descricao: "some descricao", thumbnail: "some thumbnail", titulo: "some titulo"}
    @update_attrs %{categorias: [], descricao: "some updated descricao", thumbnail: "some updated thumbnail", titulo: "some updated titulo"}
    @invalid_attrs %{categorias: nil, descricao: nil, thumbnail: nil, titulo: nil}

    def path_fixture(attrs \\ %{}) do
      {:ok, path} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Tracks.create_path()

      path
    end

    test "list_paths/0 returns all paths" do
      path = path_fixture()
      assert Tracks.list_paths() == [path]
    end

    test "get_path!/1 returns the path with given id" do
      path = path_fixture()
      assert Tracks.get_path!(path.id) == path
    end

    test "create_path/1 with valid data creates a path" do
      assert {:ok, %Path{} = path} = Tracks.create_path(@valid_attrs)
      assert path.categorias == []
      assert path.descricao == "some descricao"
      assert path.thumbnail == "some thumbnail"
      assert path.titulo == "some titulo"
    end

    test "create_path/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tracks.create_path(@invalid_attrs)
    end

    test "update_path/2 with valid data updates the path" do
      path = path_fixture()
      assert {:ok, %Path{} = path} = Tracks.update_path(path, @update_attrs)
      assert path.categorias == []
      assert path.descricao == "some updated descricao"
      assert path.thumbnail == "some updated thumbnail"
      assert path.titulo == "some updated titulo"
    end

    test "update_path/2 with invalid data returns error changeset" do
      path = path_fixture()
      assert {:error, %Ecto.Changeset{}} = Tracks.update_path(path, @invalid_attrs)
      assert path == Tracks.get_path!(path.id)
    end

    test "delete_path/1 deletes the path" do
      path = path_fixture()
      assert {:ok, %Path{}} = Tracks.delete_path(path)
      assert_raise Ecto.NoResultsError, fn -> Tracks.get_path!(path.id) end
    end

    test "change_path/1 returns a path changeset" do
      path = path_fixture()
      assert %Ecto.Changeset{} = Tracks.change_path(path)
    end
  end

  describe "waypoints" do
    alias WalkerBack.Tracks.Waypoint

    @valid_attrs %{descricao: "some descricao", pathTo: [], titulo: "some titulo"}
    @update_attrs %{descricao: "some updated descricao", pathTo: [], titulo: "some updated titulo"}
    @invalid_attrs %{descricao: nil, pathTo: nil, titulo: nil}

    def waypoint_fixture(attrs \\ %{}) do
      {:ok, waypoint} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Tracks.create_waypoint()

      waypoint
    end

    test "list_waypoints/0 returns all waypoints" do
      waypoint = waypoint_fixture()
      assert Tracks.list_waypoints() == [waypoint]
    end

    test "get_waypoint!/1 returns the waypoint with given id" do
      waypoint = waypoint_fixture()
      assert Tracks.get_waypoint!(waypoint.id) == waypoint
    end

    test "create_waypoint/1 with valid data creates a waypoint" do
      assert {:ok, %Waypoint{} = waypoint} = Tracks.create_waypoint(@valid_attrs)
      assert waypoint.descricao == "some descricao"
      assert waypoint.pathTo == []
      assert waypoint.titulo == "some titulo"
    end

    test "create_waypoint/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tracks.create_waypoint(@invalid_attrs)
    end

    test "update_waypoint/2 with valid data updates the waypoint" do
      waypoint = waypoint_fixture()
      assert {:ok, %Waypoint{} = waypoint} = Tracks.update_waypoint(waypoint, @update_attrs)
      assert waypoint.descricao == "some updated descricao"
      assert waypoint.pathTo == []
      assert waypoint.titulo == "some updated titulo"
    end

    test "update_waypoint/2 with invalid data returns error changeset" do
      waypoint = waypoint_fixture()
      assert {:error, %Ecto.Changeset{}} = Tracks.update_waypoint(waypoint, @invalid_attrs)
      assert waypoint == Tracks.get_waypoint!(waypoint.id)
    end

    test "delete_waypoint/1 deletes the waypoint" do
      waypoint = waypoint_fixture()
      assert {:ok, %Waypoint{}} = Tracks.delete_waypoint(waypoint)
      assert_raise Ecto.NoResultsError, fn -> Tracks.get_waypoint!(waypoint.id) end
    end

    test "change_waypoint/1 returns a waypoint changeset" do
      waypoint = waypoint_fixture()
      assert %Ecto.Changeset{} = Tracks.change_waypoint(waypoint)
    end
  end
end

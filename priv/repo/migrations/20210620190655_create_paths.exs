defmodule WalkerBack.Repo.Migrations.CreatePaths do
  use Ecto.Migration

  def change do
    create table(:paths) do
      add :titulo, :string
      add :descricao, :string
      add :categorias, {:array, :string}
      add :thumbnail, :binary

      timestamps()
    end

  end
end

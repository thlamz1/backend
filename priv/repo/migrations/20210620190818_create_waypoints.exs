defmodule WalkerBack.Repo.Migrations.CreateWaypoints do
  use Ecto.Migration

  def change do
    create table(:waypoints) do
      add :titulo, :string
      add :descricao, :string
      add :pathTo, {:array, :string}
      add :thumbnail, :binary
      add :path_id, references(:paths, on_delete: :delete_all)

      timestamps()
    end

    create index(:waypoints, [:path_id])
  end
end
